package com.stmare.xml;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlOpeningTagParser {
    private static final Pattern XML_ATTRIBUTE_PATTERN = Pattern.compile("(<[a-zA-Z_][^\\s>\"]*)|(([^\\s>]*)\\s*=\\s*(\"[^\"]*\"))|(/)|(>)");

    private String xml;

    public XmlOpeningTagParser(String xml) {
        this.xml = xml;
    }

    public XmlOpeningTagResult getXmlOpeningTagResult() {
        boolean finished = false;
        Matcher matcher = XML_ATTRIBUTE_PATTERN.matcher(xml);
        XmlOpeningTagResult xmlOpeningTagResult = new XmlOpeningTagResult();
        while (matcher.find()) {
            if (finished) {
                return null;
            }
            String group = matcher.group();
            if (matcher.group(1) != null) {
                xmlOpeningTagResult.setTagName(group);
            } else if (matcher.group(2) != null) {
                xmlOpeningTagResult.getAttributes().add(matcher.group(3) + "=" + matcher.group(4));
            } else if (matcher.group(5) != null) {
                xmlOpeningTagResult.setSelfclosing(true);
            } else if (matcher.group(6) != null) {
                finished = true;
            }
        }
        return xmlOpeningTagResult;
    }

    public static class XmlOpeningTagResult {
        private String tagName;

        private List<String> attributes = new ArrayList<String>();

        private boolean selfclosing;

        public String getTagName() {
            return tagName;
        }

        public void setTagName(String tagName) {
            this.tagName = tagName;
        }

        public List<String> getAttributes() {
            return attributes;
        }

        public boolean isSelfclosing() {
            return selfclosing;
        }

        public void setSelfclosing(boolean selfclosing) {
            this.selfclosing = selfclosing;
        }

        @Override
        public String toString() {
            return "XmlOpeningTagResult [tagName=" + tagName + ", attributes=" + attributes + ", selfclosing=" + selfclosing + "]";
        }
    }

    public static void main(String[] args) {
        XmlOpeningTagParser xmlOpeningTagParser = new XmlOpeningTagParser("<a8c:aaa\r\nb=\n\"c\"  x=\"nazdarek\"    >");
        XmlOpeningTagResult xmlOpeningTagResult = xmlOpeningTagParser.getXmlOpeningTagResult();
        System.out.println(xmlOpeningTagResult);
    }
}
