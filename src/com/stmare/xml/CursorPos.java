package com.stmare.xml;

public enum CursorPos {
    TAG, // somewhere in <X ... > or </X>
    COMMENT,
    CDATA, 
    OTHER, // e.g. in value section but not inside cdata
    DOCTYPE,
    HEADER
}
