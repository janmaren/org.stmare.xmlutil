package com.stmare.xml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XmlPos {
    private final static Pattern XML_ANALYZE_PATTERN = Pattern.compile("(<[a-zA-Z_])|(</[a-zA-Z_])|(<!--)|(-->)|(<!\\[CDATA\\[)|(\\]\\]>)|(>)|(<\\?)|(<!DOCTYPE[ \\t])");
    
    public static CursorPos getCursorPos(String xml, int pos) {
        boolean inTag = false;
        boolean inCdata = false;
        boolean inComment = false;
        boolean inHeader = false;
        boolean inDoctype = false;
        
        Matcher matcher = XML_ANALYZE_PATTERN.matcher(xml);
        while (matcher.find() && matcher.start() < pos) {
            int groupIndex = getGroupIndex(matcher);
            if ((groupIndex == 1 || groupIndex == 2) && !inCdata && !inComment) {
                inTag = true;
            } else if (groupIndex == 7 && (inTag || inHeader || inDoctype) && !inCdata && !inComment) {
                inTag = false;
                inHeader = false;
                inDoctype = false;
            } else if (groupIndex == 1 && !inCdata && !inComment) {
                inTag = true;
            } else if (groupIndex == 3 && !inCdata) {
                if (inTag) {
                    System.err.println("Found comment but in it's in tag");
                    return null;
                }
                inComment = true;
            } else if (groupIndex == 4 && !inCdata) {
                if (inTag) {
                    System.err.println("Found comment but in it's in tag");
                    return null;
                }
                inComment = false;
            } else if (groupIndex == 5 && !inComment) {
                if (inTag) {
                    System.err.println("Found cdata but in it's in tag");
                    return null;
                }
                inCdata = true;
            } else if (groupIndex == 6 && !inComment) {
                if (inTag) {
                    System.err.println("Found cdata but in it's in tag");
                    return null;
                }
                inCdata = false;
            } else if (groupIndex == 8 && !inCdata && !inComment) {
                inHeader = true;
            } else if (groupIndex == 9 && !inCdata && !inComment) {
                inDoctype = true;
            } 

        }
        
        if (inTag) {
            return CursorPos.TAG;
        }
        if (inCdata) {
            return CursorPos.CDATA;
        }
        if (inComment) {
            return CursorPos.COMMENT;
        }
        if (inDoctype) {
            return CursorPos.DOCTYPE;
        }
        if (inHeader) {
            return CursorPos.HEADER;
        }
        return CursorPos.OTHER;
    }

    public static int findGtPosBackwardCareful(String xml, int pos) {
        boolean inTag = false;
        boolean inCdata = false;
        boolean inComment = false;
        boolean inHeader = false;
        boolean inDoctype = false;
        
        int lastPos = -1;
        Matcher matcher = XML_ANALYZE_PATTERN.matcher(xml);
        while (matcher.find() && matcher.start() < pos) {
            int groupIndex = getGroupIndex(matcher);
            if ((groupIndex == 1 || groupIndex == 2) && !inCdata && !inComment) {
                inTag = true;
            } else if (groupIndex == 7 && (inTag || inHeader || inDoctype) && !inCdata && !inComment) {
                inTag = false;
                inHeader = false;
                inDoctype = false;
                lastPos = matcher.start() + 1;
            } else if (groupIndex == 3 && !inCdata) {
                inComment = true;
            } else if (groupIndex == 4 && !inCdata) {
                inComment = false;
            } else if (groupIndex == 5 && !inComment) {
                inCdata = true;
            } else if (groupIndex == 6 && !inComment) {
                inCdata = false;
            } else if (groupIndex == 8 && !inCdata && !inComment) {
                inHeader = true;
            } else if (groupIndex == 9 && !inCdata && !inComment) {
                inDoctype = true;
            } 
        }
        
        return lastPos;
    }
    
    public static int findLtPosForwardCareful(String xml, int pos) {
        boolean inTag = false;
        boolean inCdata = false;
        boolean inComment = false;
        boolean inHeader = false;
        boolean inDoctype = false;
        
        Matcher matcher = XML_ANALYZE_PATTERN.matcher(xml);
        while (matcher.find()) {
            int groupIndex = getGroupIndex(matcher);
            if ((groupIndex == 1 || groupIndex == 2) && !inCdata && !inComment) {
                if (matcher.start() >= pos) {
                    return matcher.start();
                }
                inTag = true;
            } else if (groupIndex == 7 && (inTag || inHeader || inDoctype) && !inCdata && !inComment) {
                inTag = false;
                inHeader = false;
                inDoctype = false;
            } else if (groupIndex == 3 && !inCdata) {
                inComment = true;
            } else if (groupIndex == 4 && !inCdata) {
                inComment = false;
            } else if (groupIndex == 5 && !inComment) {
                inCdata = true;
            } else if (groupIndex == 6 && !inComment) {
                inCdata = false;
            } else if (groupIndex == 8 && !inCdata && !inComment) {
                if (matcher.start() >= pos) {
                    return matcher.start();
                }
                inHeader = true;
            } else if (groupIndex == 9 && !inCdata && !inComment) {
                if (matcher.start() >= pos) {
                    return matcher.start();
                }
                inDoctype = true;
            } 
        }
        
        return -1;
    }
    
    public static int getGroupIndex(Matcher matcher) {
        for (int i = 1; i <= matcher.groupCount(); i++) {
            if (matcher.group(i) != null) {
                return i;
            }
        }
        throw new RuntimeException("Not found group for " + matcher.group());
    }
    
    public static void main(String[] args) {
        String xml = "<?xml><a>  <b></b><c><![CDATA[neco <!-- pozn -->]]> </c><c2><!--<![CDATA[xxx]]>--></c2>";
        CursorPos cursorPos = getCursorPos(xml, xml.indexOf("xx"));
        System.out.println(cursorPos);
    }
}
