package com.stmare.xml.format;

import com.stmare.str.LineStrUtil;
import com.stmare.str.StrUtil;
import com.stmare.xml.CursorPos;
import com.stmare.xml.XMLStrUtil;
import com.stmare.xml.XmlPos;

public class RangeXmlFormatter {
    private final String xml;
    
    private final int selectionStart;
    
    private final int selectionLength;

    private XmlFormatConfig xmlFormatConfig;

    private XmlFormatter xmlFormatter;
    
    private static final String MARKER_START = "fgsfasA";

    private static final String MARKER_END = "fgsfasB";
    
    public RangeXmlFormatter(String xml, int selectionStart, int selectionLength) {
        super();
        this.xml = xml;
        this.selectionStart = selectionStart;
        this.selectionLength = selectionLength;
    }
    
    public RangeXmlFormatter(String xml, int selectionStart, int selectionLength, XmlFormatConfig xmlFormatConfig) {
        this(xml, selectionStart, selectionLength);
        this.xmlFormatConfig = xmlFormatConfig;
    }

    public RangeXmlFormatter(String xml, int selectionStart, int selectionLength, XmlFormatter xmlFormatter) {
        this(xml, selectionStart, selectionLength);
        this.xmlFormatter = xmlFormatter;
    }
    
    public XmlFormatResult getFormattedXml() {
        XmlFormatResult xmlFormatResult = new XmlFormatResult();
        
        String markerStart = XMLStrUtil.findNonConflictMarker(xml, MARKER_START);
        String markerEnd = XMLStrUtil.findNonConflictMarker(xml, MARKER_END);
        if (markerStart == null || markerEnd == null) {
            return xmlFormatResult;
        }
        
        int startMovedDelta; // right positive, left negative
        int endMovedDelta; // right positive, left negative
        
        int findPosBehindHeader = XMLStrUtil.findPosBehindHeader(xml, selectionStart);
        boolean inHeaderCase = findPosBehindHeader != -1 && selectionStart < findPosBehindHeader;
        if (inHeaderCase) {
            startMovedDelta = findPosBehindHeader - selectionStart;
        } else {
            CursorPos cursorPos = XmlPos.getCursorPos(xml, selectionStart);
            if (cursorPos == CursorPos.TAG || cursorPos == CursorPos.HEADER || cursorPos == CursorPos.DOCTYPE) {
                int findGtPosForward = XMLStrUtil.findGtPosForward(xml, selectionStart);
                startMovedDelta = findGtPosForward - selectionStart;
            } else {
                int findNonWhiteSpacePosBackward = XmlPos.findGtPosBackwardCareful(xml, selectionStart); //XMLStrUtil.findNonWhiteSpacePosBackward(xml, selectionStart);
                if (findNonWhiteSpacePosBackward == -1) {
                    findNonWhiteSpacePosBackward = 0;
                }
                startMovedDelta = findNonWhiteSpacePosBackward - selectionStart;
            }
        }
        
        {
            int selectionEnd = selectionStart + selectionLength;
            CursorPos cursorPos = XmlPos.getCursorPos(xml, selectionEnd);
            if (cursorPos == CursorPos.TAG || cursorPos == CursorPos.HEADER || cursorPos == CursorPos.DOCTYPE) {
                int newPos = XMLStrUtil.findLtPosBackward(xml, selectionEnd);
                endMovedDelta = newPos - selectionEnd;
            } else {
                int findNonWhiteSpacePosForward = XmlPos.findLtPosForwardCareful(xml, selectionEnd); //XMLStrUtil.findNonWhiteSpacePosForward(xml, selectionEnd);
                if (findNonWhiteSpacePosForward == -1) {
                    findNonWhiteSpacePosForward = xml.length();
                }
                endMovedDelta = findNonWhiteSpacePosForward - selectionEnd;
            }
        }
        
        int newStart = selectionStart + startMovedDelta;
        int newEnd = selectionStart + selectionLength + endMovedDelta;
        if (newEnd <= newStart) {
            return xmlFormatResult;
        }

        
        // pridani markeru
        String xmlToFormat = xml.substring(0, newStart) + markerStart + xml.substring(newStart);
        xmlToFormat = xmlToFormat.substring(0, newEnd + markerStart.length()) + markerEnd + xmlToFormat.substring(newEnd + markerStart.length());
        
        // formatovani
        if (xmlFormatter == null) {
            xmlFormatter = new TransformerXmlFormatter();
            xmlFormatter.setXmlFormatConfig(xmlFormatConfig);
        }
        String wholeXml = xmlFormatter.getFormattedXml(xmlToFormat);
        if (wholeXml == null) {
            xmlFormatResult.setResultMessage(xmlFormatter.getResultMessage());
            return xmlFormatResult;
        }
        String newXml = wholeXml;
        
        // hledani markeru, poznaceni pozice a vymazani markeru
        int start = -1;
        int end = -1;

        {
            int indexOf = newXml.indexOf(markerStart);
            if (indexOf == -1) {
                return xmlFormatResult;
            }
            {
                boolean whiteSpaceAroundStart = XMLStrUtil.isWhiteSpaceAround(newXml, indexOf, markerStart.length());
                int findNonWhiteSpacePosForward = StrUtil.findNonWhiteSpacePosForward(newXml, indexOf + markerStart.length());
                int findNonWhiteSpacePosBackward = StrUtil.findNonWhiteSpacePosBackward(newXml, indexOf) + 1;
                if (inHeaderCase) {
                    start = findPosBehindHeader;
                    newXml = newXml.substring(0, indexOf) + newXml.substring(findNonWhiteSpacePosForward);
                } else if (whiteSpaceAroundStart) {
                    start = findNonWhiteSpacePosBackward;
                    int[] lineRangeForPos = LineStrUtil.getLineRangeForPos(newXml, indexOf);
                    newXml = newXml.substring(0, lineRangeForPos[0]) + newXml.substring(lineRangeForPos[1]);
                } else {
                    start = findNonWhiteSpacePosBackward;
                    newXml = newXml.substring(0, findNonWhiteSpacePosBackward) + newXml.substring(findNonWhiteSpacePosForward);
                }
            }
        }
        
        {
            int indexOf = newXml.indexOf(markerEnd);
            if (indexOf == -1) {
                return xmlFormatResult;
            }
            boolean whiteSpaceAroundStart = XMLStrUtil.isWhiteSpaceAround(newXml, indexOf, markerEnd.length());
            int findNonWhiteSpacePosForward = StrUtil.findNonWhiteSpacePosForward(newXml, indexOf + markerEnd.length());
            if (whiteSpaceAroundStart) {
                int[] lineRangeForPos = LineStrUtil.getLineRangeForPos(newXml, indexOf);
                //end = findNonWhiteSpacePosForward - (indexOf + markerEnd.length() - findNonWhiteSpacePosBackward);
                end = findNonWhiteSpacePosForward - (lineRangeForPos[1] - lineRangeForPos[0]);
                //newXml = newXml.substring(0, findNonWhiteSpacePosBackward) + newXml.substring(indexOf + markerEnd.length());
                newXml = newXml.substring(0, lineRangeForPos[0]) + newXml.substring(lineRangeForPos[1]);
            } else {
                end = indexOf;
                newXml = newXml.substring(0, indexOf) + newXml.substring(findNonWhiteSpacePosForward);
            }
        }
        
        xmlFormatResult.setFormattedXmlSelectionStart(inHeaderCase ? 0 : start);
        xmlFormatResult.setFormattedXmlSelectionEnd(end);
        xmlFormatResult.setFormattedXmlWhole(newXml);
        xmlFormatResult.setStartMovedDelta(inHeaderCase ? -selectionStart :startMovedDelta);
        xmlFormatResult.setEndMovedDelta(endMovedDelta);
        
        return xmlFormatResult;
    }
    
    public static void main(String[] args) {
        //String xml = "<a attr1=\"dlouhyyyyyyyyyyyyy\" attr2=\"dlouhyyyyyyyyyyyyy\" attr3=\"dlouhyyyyyyyyyyyyy\" attr4=\"dlouhyyyyyyyyyyyyy\"><b> </b></a>";
        //String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<a><b>\n\n<c/><d>text1<!-- middle -->\n <!-- middle2 -->text2</d><e value=\"0\"/>\n<tag><![CDATA[ a\n\nb\n c ]]></tag>\n\n<f></f> </b></a>";
//        String xml = "<?xml version=\"1.0\"?>\n<!DOCTYPE module PUBLIC \"-//Puppy Crawl//DTD Check Configuration 1.3//EN\" " +
//        "\"http://www.puppycrawl.com/dtds/configuration_1_3.dtd\">\n<a><b>\n\n<c/><d>text1<!-- middle \n\n -->\n <!-- middle2 -->text2</d><e value=\"0\"/>\n</b><tag><![CDATA[ a\n\nb\n c ]]></tag>\n\n<f></f>  middle  </a>";
        String xml = "<a>\n\n    <b>nejaky text</b>\n\n    <b2></b2></a>\n";
        System.out.println(xml);
        System.out.println("-------------------");
        int indexOf = xml.indexOf("<b>");
        int selectionLength = xml.indexOf("</b>") + 5 - indexOf;
        RangeXmlFormatter rangeXmlFormatter = new RangeXmlFormatter(xml, indexOf, selectionLength, new XmlFormatConfig().setIndent(4).setLineFeed("\r\n").setKeepEmptyLines(true));
        XmlFormatResult prettyFormatRange = rangeXmlFormatter.getFormattedXml();//prettyFormatRange(xml, 4, "\r\n", indexOf, selectionLength);
        System.out.println(prettyFormatRange);
        if (prettyFormatRange.isFormatted()) {
            System.out.println("==============");
            System.out.println(prettyFormatRange.getFormattedXmlSelection());
            System.out.println("==============");
        }
        
//        System.out.println(prettyFormat(xml, 4, "\n"));
    }    
}
