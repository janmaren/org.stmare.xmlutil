package com.stmare.xml.format;

public interface XmlFormatter {
    /**
     * Vraci formatovany text nebo null, pokud se nedari zformatovat
     * @param xml
     * @return
     */
    public String getFormattedXml(String xml);
    
    public void setXmlFormatConfig(XmlFormatConfig xmlFormatConfig);
    
    public String getResultMessage();
}
