package com.stmare.xml.format;

public class XmlFormatConfig {
    private int indent = 4;
    
    private boolean useTabs = false;
    
    private String lineFeed = null;
    
    private boolean keepEmptyLines;
    
    public int getIndent() {
        return indent;
    }

    public XmlFormatConfig setIndent(int indent) {
        this.indent = indent;
        return this;
    }

    public boolean isUseTabs() {
        return useTabs;
    }

    public XmlFormatConfig setUseTabs(boolean useTabs) {
        this.useTabs = useTabs;
        return this;
    }

    public String getLineFeed() {
        return lineFeed != null ? lineFeed : System.getProperty("line.separator");
    }

    public XmlFormatConfig setLineFeed(String lineFeed) {
        this.lineFeed = lineFeed;
        return this;
    }

    public boolean isKeepEmptyLines() {
        return keepEmptyLines;
    }

    public XmlFormatConfig setKeepEmptyLines(boolean keepEmptyLines) {
        this.keepEmptyLines = keepEmptyLines;
        return this;
    }
}
