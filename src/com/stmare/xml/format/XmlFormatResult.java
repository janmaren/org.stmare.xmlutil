package com.stmare.xml.format;

public class XmlFormatResult {
    private String formattedXmlWhole;
    
    private int formattedXmlSelectionStart = -1;
    
    private int formattedXmlSelectionEnd = -1;

    private int startMovedDelta;

    private int endMovedDelta;

    private String resultMessage;

    public String getFormattedXmlSelection() {
        return formattedXmlWhole.substring(formattedXmlSelectionStart, formattedXmlSelectionEnd);
    }

    public String getFormattedXmlWhole() {
        return formattedXmlWhole;
    }

    public void setFormattedXmlWhole(String formattedXmlWhole) {
        this.formattedXmlWhole = formattedXmlWhole;
    }

    public int getFormattedXmlSelectionStart() {
        return formattedXmlSelectionStart;
    }

    public void setFormattedXmlSelectionStart(int formattedXmlSelectionStart) {
        this.formattedXmlSelectionStart = formattedXmlSelectionStart;
    }

    public int getFormattedXmlSelectionEnd() {
        return formattedXmlSelectionEnd;
    }

    public void setFormattedXmlSelectionEnd(int formattedXmlSelectionEnd) {
        this.formattedXmlSelectionEnd = formattedXmlSelectionEnd;
    }
    
    public boolean isFormatted() {
        return formattedXmlWhole != null && formattedXmlSelectionStart != -1 && formattedXmlSelectionEnd != -1;
    }

    @Override
    /**
     * For debug
     */
    public String toString() {
        String substring = null;
        if (formattedXmlSelectionStart != -1 && formattedXmlSelectionEnd != -1 ) {
            substring = formattedXmlWhole.substring(formattedXmlSelectionStart, formattedXmlSelectionEnd);
        } else {
            substring = "#UNKNOWN#";
        }
        return startMovedDelta + "{" + substring + "}" + endMovedDelta;
    }

    public void setStartMovedDelta(int startMovedDelta) {
        this.startMovedDelta = startMovedDelta;
    }

    public int getStartMovedDelta() {
        return startMovedDelta;
    }

    public void setEndMovedDelta(int endMovedDelta) {
        this.endMovedDelta = endMovedDelta;
    }

    public int getEndMovedDelta() {
        return endMovedDelta;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public String getResultMessage() {
        return resultMessage;
    }
}