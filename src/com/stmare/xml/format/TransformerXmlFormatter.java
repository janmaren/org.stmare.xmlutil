package com.stmare.xml.format;

import java.io.ByteArrayInputStream;
import java.io.StringWriter;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXParseException;

import com.stmare.regexp.ReplaceCallback;
import com.stmare.regexp.ReplaceCallback.Callback;
import com.stmare.str.LineStrUtil;
import com.stmare.xml.XMLStrUtil;
import com.stmare.xml.XmlOpeningTagGenerator;

public class TransformerXmlFormatter implements XmlFormatter {
    private XmlFormatConfig xmlFormatConfig;

    private String resultMessage;

    private final static Pattern HEADER_ELEMENTS = Pattern.compile("(<.*?>)|(^[ \\t]*?$)", Pattern.DOTALL | Pattern.MULTILINE);

    @Override
    public String getFormattedXml(String xml) {
        if (xmlFormatConfig == null) {
            xmlFormatConfig = new XmlFormatConfig();
        }
        String formattableXml = xml;
        int firstTagIndex = XMLStrUtil.findFirstTag(xml);
        String headerPart = null;
        if (firstTagIndex != -1) {
            headerPart = formatHeader(formattableXml.substring(0, firstTagIndex));
            formattableXml = formattableXml.substring(firstTagIndex);
        }

        String nonConflictMarker = null;
        if (xmlFormatConfig.isKeepEmptyLines()) {
            nonConflictMarker = XMLStrUtil.findNonConflictMarker(formattableXml, "empty");
            formattableXml = insertMarkerToEmptyLines(formattableXml, nonConflictMarker);
        }
        try {
            Document document = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder()
                    .parse(new InputSource(new ByteArrayInputStream(formattableXml.getBytes("utf-8"))));

            XPath xPath = XPathFactory.newInstance().newXPath();
            NodeList nodeList = (NodeList) xPath.evaluate("//text()[normalize-space()='']",
                                                          document,
                                                          XPathConstants.NODESET);

            for (int i = 0; i < nodeList.getLength(); ++i) {
                Node node = nodeList.item(i);
                node.getParentNode().removeChild(node);
            }

            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", String.valueOf(xmlFormatConfig.getIndent()));

            StringWriter stringWriter = new StringWriter();
            StreamResult streamResult = new StreamResult(stringWriter);

            String systemSeparator = System.getProperty("line.separator");
            transformer.transform(new DOMSource(document), streamResult);

            String string = stringWriter.toString();
            String lineFeed = xmlFormatConfig.getLineFeed();
            if (lineFeed != null && !systemSeparator.equals(lineFeed)) {
                string = string.replace(systemSeparator, lineFeed);
            }
            string = formatOpeningTags(string);
            if (headerPart != null && headerPart.trim().length() > 0) {
                string = headerPart + string;
            }

            if (xmlFormatConfig.isUseTabs()) {
                string = leadingSpacesToTabs(string, xmlFormatConfig.getIndent());
            }

            if (xmlFormatConfig.isKeepEmptyLines()) {
                string = string.replaceAll("(?m)^.*" + nonConflictMarker, "");
            }

            return string;
        }
        catch (SAXParseException e) {
            int lineNumber = e.getLineNumber() + (headerPart != null ? countLineNumbers(headerPart) - 1 : 0);
            resultMessage = "Line: " + lineNumber + ", Column: " + e.getColumnNumber() + " - " + e.getMessage();
            return null;
        }
        catch (Exception e) {
            resultMessage = e.getMessage();
            return null;
        }
    }

    private String formatOpeningTags(final String xml) {
        Pattern XML_ANALYZE_PATTERN = Pattern.compile("(<!--)|(-->)|(<!\\[CDATA\\[)|(\\]\\]>)|(<(\"[^\"]*\"|'[^']*'|[^'\">])*>)");
        String result = ReplaceCallback.replace(XML_ANALYZE_PATTERN, xml, new Callback(){

            boolean inCdata = false;
            boolean inComment = false;

            @Override
            public String matchFound(MatchResult match) {
                int groupIndex = getGroupIndex(match);
                if (groupIndex == 5 && !inCdata && !inComment) {
                    String group = match.group();
                    if (group.charAt(1) != '/' && group.charAt(1) != '?' && group.charAt(1) != '!') {
                        String attributePart = xml.substring(match.start(), match.end());
                        int[] lineRangeForPos = LineStrUtil.getLineRangeForPos(xml, match.start());
                        XmlOpeningTagGenerator xmlOpeningTagGenerator = new XmlOpeningTagGenerator(attributePart, match.start() - lineRangeForPos[0], -1, xmlFormatConfig.getLineFeed());
                        //xmlOpeningTagGenerator.setFirstOfMultipleOnNewLine(true);
                        return xmlOpeningTagGenerator.getOpeningTag();
                    }
                } else if (groupIndex == 1 && !inCdata) {
                    inComment = true;
                } else if (groupIndex == 2 && !inCdata) {
                    inComment = false;
                } else if (groupIndex == 3 && !inComment) {
                    inCdata = true;
                } else if (groupIndex == 4 && !inComment && inCdata) {
                    inCdata = false;
                }

                return match.group();
            }

            public int getGroupIndex(MatchResult matcher) {
                for (int i = 1; i <= matcher.groupCount(); i++) {
                    if (matcher.group(i) != null) {
                        return i;
                    }
                }
                throw new RuntimeException("Not found group for " + matcher.group());
            }
        });
        return result;
    }

    private String formatHeader(String string) {
        StringBuilder sb = new StringBuilder();
        Matcher matcher = HEADER_ELEMENTS.matcher(string);
        while (matcher.find()) {
            if (matcher.group(1) != null) {
                String group = matcher.group();
                sb.append(group);
            }
            sb.append(xmlFormatConfig.getLineFeed());
        }
        return sb.toString();
    }

    @Override
    public void setXmlFormatConfig(XmlFormatConfig xmlFormatConfig) {
        this.xmlFormatConfig = xmlFormatConfig;
    }

    public static String leadingSpacesToTabs(String xml, final int spaceSize) {
        String result = ReplaceCallback.replace(Pattern.compile("^ +", Pattern.MULTILINE), xml, new Callback(){
            @Override
            public String matchFound(MatchResult match) {
                StringBuilder sb = new StringBuilder();
                String group = match.group();
                int length = group.length();
                int tabsNumber = length / spaceSize;
                for (int i = 0; i < tabsNumber; i++) {
                    sb.append("\t");
                }
                int spacesNumber = length % spaceSize;
                for (int i = 0; i < spacesNumber; i++) {
                    sb.append(" ");
                }
                return sb.toString();
            }});
        return result;
    }

    public static String insertMarkerToEmptyLines(String xml, final String marker) {
        String result = ReplaceCallback.replace(Pattern.compile("(^ *$)|(<!--)|(-->)|(<[a-zA-Z_])|(>)", Pattern.MULTILINE), xml, new Callback(){
            private boolean inRemark;

            private boolean inTag;

            @Override
            public String matchFound(MatchResult match) {
                String group = match.group();
                if (match.group(2) != null) {
                    inRemark = true;
                    return group;
                } else if (match.group(3) != null) {
                    inRemark = false;
                    return group;
                } else if (match.group(4) != null && !inRemark) {
                    inTag = true;
                    return group;
                } else if (match.group(5) != null && !inRemark) {
                    inTag = false;
                    return group;
                } else {
                    if (inRemark || inTag) {
                        return group;
                    }
                    return marker;
                }
            }});
        return result;
    }

    /**
     * !!Prepsat podporujic MAC
     * @param str
     * @return
     */
    @Deprecated
    public static int countLineNumbers(String str) {
        if (str == null || str.length() == 0) {
            return 1;
        }
        int count = 1;
        int idx = 0;
        while ((idx = str.indexOf("\n", idx)) != -1) {
            count++;
            idx ++;
        }
        return count;
    }

    public static void main(String[] args) {
        TransformerXmlFormatter transformerXmlFormatter = new TransformerXmlFormatter();
        String formattedXml = transformerXmlFormatter.getFormattedXml("<a>  <b x=\"y\" z=\"z\" d=\"z\" e=\"z\" id=\"idecko\"> </b></a>");
        System.out.println(formattedXml);
    }

    @Override
    public String getResultMessage() {
        return resultMessage;
    }
}
