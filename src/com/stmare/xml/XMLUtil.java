package com.stmare.xml;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.DocumentType;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class XMLUtil {
    private static DocumentBuilder documentBuilder;

    public static DocumentBuilder getDocumentBuilder() {
        if (documentBuilder == null) {
            try {
                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                factory.setValidating(false);
                factory.setNamespaceAware(false);
                factory.setExpandEntityReferences(false);
                try {
                    factory.setFeature("http://apache.org/xml/features/allow-java-encodings", true);
                } catch (ParserConfigurationException localParserConfigurationException) {
                }

                documentBuilder = factory.newDocumentBuilder();
                documentBuilder.setEntityResolver(new EntityResolver() {
                    public InputSource resolveEntity(String publicId, String systemId) throws SAXException, IOException {
                        return new InputSource(new ByteArrayInputStream(new byte[0]));
                    }
                });
            } catch (Exception e) {
                throw new RuntimeException("Unable to create document", e);
            }
        }
        return documentBuilder;
    }

    public static Element createChildElement(Document doc, Element element, int index, String nodeName) {
        Element element2 = doc.createElement(nodeName);
        try {
            NodeList childList = element.getElementsByTagName(nodeName);
            Node child = childList.item(index);
            element.insertBefore(element2, child);
        } catch (Exception localException) {
            element.appendChild(element2);
        }
        return element2;
    }

    public static Element createChildElement(Document doc, Node node, String nodeName) {
        Element element = doc.createElement(nodeName);
        node.appendChild(element);
        return element;
    }

    public static void createTextChildElement(Document doc, Node node, String name, String value) {
        Element element = createChildElement(doc, node, name);
        element.appendChild(doc.createTextNode(value));
    }

    public static String getAttributeValue(Element element, String attr) {
        return element.getAttributeNode(attr).getValue();
    }

    public static byte[] getContents(Document document) throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            print(new PrintStream(out, true, "UTF-8"), document);
            return out.toByteArray();
        } catch (Exception ex) {
            throw new IOException(ex.getLocalizedMessage());
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (Exception localException2) {
                }
        }
    }

    protected static String getDocumentTypeData(DocumentType doctype) {
        String data = doctype.getName();
        if (doctype.getPublicId() != null) {
            data = data + " PUBLIC \"" + doctype.getPublicId() + "\"";
            String systemId = doctype.getSystemId();
            if (systemId == null)
                systemId = "";
            data = data + " \"" + systemId + "\"";
        } else {
            data = data + " SYSTEM \"" + doctype.getSystemId() + "\"";
        }
        return data;
    }

    public static Iterator<Node> getNodeIterator(Element element, String name) {
        List<Node> list = new ArrayList<Node>();
        NodeList nodeList = element.getElementsByTagName(name);

        int length = nodeList.getLength();
        for (int i = 0; i < length; i++) {
            list.add(nodeList.item(i));
        }
        return list.iterator();
    }

    public static String getNodeValue(Node node) {
        NodeList nodeList = node.getChildNodes();

        int length = nodeList.getLength();
        for (int i = 0; i < length; i++) {
            Node n = nodeList.item(i);
            if ((n instanceof Text)) {
                Text t = (Text) n;
                return t.getNodeValue();
            }
        }
        return "";
    }

    public static String getSubNodeValue(Element element, String name) {
        NodeList nodeList = element.getElementsByTagName(name);
        return getNodeValue(nodeList.item(0)).trim();
    }

    public static void insertText(Document doc, Node node, String text) {
        node.appendChild(doc.createCDATASection(text));
    }

    protected static String normalize(String s) {
        StringBuffer stringbuffer = new StringBuffer();
        int i = s == null ? 0 : s.length();
        for (int j = 0; j < i; j++) {
            char c = s.charAt(j);
            switch (c) {
            case '<':
                stringbuffer.append("&lt;");
                break;
            case '>':
                stringbuffer.append("&gt;");
                break;
            case '&':
                stringbuffer.append("&amp;");
                break;
            case '"':
                stringbuffer.append("&quot;");
                break;
            case '\n':
            case '\r':
            default:
                stringbuffer.append(c);
            }

        }

        return stringbuffer.toString();
    }

    protected static void print(PrintStream out, Node node) {
        if (node == null)
            return;
        short type = node.getNodeType();
        switch (type) {
        case 9:
            out.println("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

            NodeList nodelist = node.getChildNodes();
            int size = nodelist.getLength();
            for (int i = 0; i < size; i++)
                print(out, nodelist.item(i));
            break;
        case 10:
            DocumentType docType = (DocumentType) node;
            out.print("<!DOCTYPE " + getDocumentTypeData(docType) + ">\n");
            break;
        case 1:
            out.print('<');
            out.print(node.getNodeName());
            NamedNodeMap map = node.getAttributes();
            if (map != null) {
                int size2 = map.getLength();
                for (int i = 0; i < size2; i++) {
                    Attr attr = (Attr) map.item(i);
                    out.print(' ');
                    out.print(attr.getNodeName());
                    out.print("=\"");
                    out.print(normalize(attr.getNodeValue()));
                    out.print('"');
                }
            }

            if (!node.hasChildNodes()) {
                out.print("/>");
            } else {
                out.print('>');
                NodeList nodelist2 = node.getChildNodes();
                int numChildren = nodelist2.getLength();
                for (int i = 0; i < numChildren; i++) {
                    print(out, nodelist2.item(i));
                }
                out.print("</");
                out.print(node.getNodeName());
                out.print('>');
            }
            break;
        case 5:
            NodeList nodelist3 = node.getChildNodes();
            if (nodelist3 != null) {
                int size3 = nodelist3.getLength();
                for (int i = 0; i < size3; i++) {
                    print(out, nodelist3.item(i));
                }
            }
            break;
        case 4:
            out.print(normalize(node.getNodeValue()));
            break;
        case 3:
            out.print(normalize(node.getNodeValue()));
            break;
        case 7:
            out.print("<?");
            out.print(node.getNodeName());
            String s = node.getNodeValue();
            if ((s != null) && (s.length() > 0)) {
                out.print(' ');
                out.print(s);
            }
            out.print("?>");
            break;
        case 8:
            out.print("<!--");
            out.print(node.getNodeValue());
            out.print("-->");
            break;
        case 2:
        case 6:
        default:
            out.print(normalize(node.getNodeValue()));
        }

        out.flush();
    }

    public static void save(String filename, Document document) throws IOException {
        PrintStream out = null;
        try {
            out = new PrintStream(new BufferedOutputStream(new FileOutputStream(filename)), true, "UTF-8");

            print(out, document);
        } catch (Exception ex) {
            throw new IOException(ex.getLocalizedMessage());
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (Exception localException1) {
                }
        }
    }

    public static void save(String filename, Node node) throws IOException {
        PrintStream out = null;
        try {
            out = new PrintStream(new BufferedOutputStream(new FileOutputStream(filename)), true, "UTF-8");
            print(out, node);
        } catch (Exception ex) {
            throw new IOException(ex.getLocalizedMessage());
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (Exception localException1) {
                }
        }
    }

    public static void setNodeValue(Node node, String name, String value) {
        String s = node.getNodeValue();
        if (s != null) {
            node.setNodeValue(value);
            return;
        }
        NodeList nodelist = node.getChildNodes();
        for (int i = 0; i < nodelist.getLength(); i++)
            if ((nodelist.item(i) instanceof Text)) {
                Text text = (Text) nodelist.item(i);
                text.setData(value);
                return;
            }
    }

    public static String toString(Document document) {
        PrintStream out = null;
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream(2048);
            out = new PrintStream(baos);
            print(out, document);
            return new String(baos.toByteArray(), "UTF-8");
        } catch (Exception localException2) {
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (Exception localException4) {
                }
        }
        return null;
    }
}