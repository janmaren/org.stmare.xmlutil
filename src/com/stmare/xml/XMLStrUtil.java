package com.stmare.xml;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.stmare.str.StrUtil;

public class XMLStrUtil {
    private static final Pattern LETTER_PATTERN = Pattern.compile("[A-Za-z_]");
    
    /**
     * Vraci true pokud je pozice v tagu
     * Pokud jsme na &lt;, tak jsme pred tagem (mimo tag)
     * Pokud jsme jednu pozici za &lt;, tak jsme v tagu
     * Pokud jsme na &gt;, tak jsme stale uvnitr tagu
     * Pokud jsme za &gt;, tak jsme mimo tag
     * @param str
     * @param pos
     * @return
     */
    public static boolean isPosInsideTag(String xml, int pos)
    {
        return XmlPos.getCursorPos(xml, pos) == CursorPos.TAG;
    }
    
    /**
     * Pokud nejsme v hlavicce, vrati -1, jinak prvni ne whitespace pozice za hlavickou
     * @param str
     * @param pos
     * @return
     */
    public static int findPosBehindHeader(String str, int pos) {
        int indexOf = str.indexOf("<?xml");
        if (indexOf == -1) {
            return -1;
        }
        indexOf = str.indexOf(">", indexOf);
        if (indexOf == -1) {
            return -1;
        }
        return indexOf + 1;
    }
    
    public static boolean isPosInsideCdata(String xml, int pos) {
        return XmlPos.getCursorPos(xml, pos) == CursorPos.CDATA;
    }

    /**
     * Najde prvni pozici za tagem, nebo koncovou pozici souboru
     * Mozno pouzit jen pokud jsem urcite v tagu, napr. po operaci {@link XmlPos#getCursorPos(String, int)}
     * @param str
     * @param fromPos
     * @return
     */
    public static int findGtPosForward(String str, int fromPos) {
        while (fromPos < str.length() && str.charAt(fromPos) != '>') {
            fromPos ++;
        }
        return fromPos == str.length() ? str.length() : fromPos + 1;
    }

    /**
     * Najde pozici pred tagem (coz je pocatek tagu)
     * Mozno pouzit jen pokud jsem urcite v tagu, napr. po operaci {@link XmlPos#getCursorPos(String, int)}
     * @param str
     * @param fromPos
     * @return
     */
    public static int findLtPosBackward(String str, int fromPos) {
        while (fromPos > 0 && str.charAt(fromPos) != '<') {
            fromPos --;
        }
        return fromPos;
    }
    
    /**
     * Vraci true pokud je kolem substringu omezeneho pomoci start a length white space
     * @param newXml
     * @param start
     * @param length
     * @return
     */
    public static boolean isWhiteSpaceAround(String newXml, int start, int length) {
        if (start > 0 && !StrUtil.isWhitespace(newXml.charAt(start - 1))) {
            return false;
        }
        if (newXml.length() > start + length && !StrUtil.isWhitespace(newXml.charAt(start + length))) {
            return false;
        }
        return true;
    }
    
    public static int findFirstTag(String str) {
        int untilIndex = str.length() - 1;
        int i = -1;
        while (++i < untilIndex) {
            char charAt = str.charAt(i);
            if (charAt == '<') {
                char charAt1 = str.charAt(i + 1);
                Matcher matcher = LETTER_PATTERN.matcher(String.valueOf(charAt1));
                if (matcher.find()) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static String findNonConflictMarker(String xml, String marker) {
        String tryMarker = "<!-- " + marker + " -->";
        if (xml.contains(tryMarker)) {
            for (int i = 0; i < 1000; i++) {
                tryMarker = "<!-- " + marker + i + " -->";
                if (!xml.contains(tryMarker)) {
                    return tryMarker;
                }
            }
            return null;
        }
        return tryMarker;
    }
}
