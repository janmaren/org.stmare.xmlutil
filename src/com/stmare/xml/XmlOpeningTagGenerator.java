package com.stmare.xml;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.stmare.xml.XmlOpeningTagParser.XmlOpeningTagResult;

public class XmlOpeningTagGenerator {
    private static String[] MAIN_ATTR_NAMES = {"xmlns", "xmlns:*", "id", "name", "class"};

    private int indent;

    private int indentAttr;

    private String openingTagPart;

    private String lineSeparator;

    private boolean firstOfMultipleOnNewLine;

    public XmlOpeningTagGenerator(String openingTagPart, int indent, int indentAttr, String lineSeparator) {
        super();
        this.openingTagPart = openingTagPart;
        this.indent = indent;
        this.indentAttr = indentAttr;
        this.lineSeparator = lineSeparator;
    };

    public String getOpeningTag() {
        XmlOpeningTagParser xmlOpeningTagParser = new XmlOpeningTagParser(openingTagPart);
        XmlOpeningTagResult xmlOpeningTagResult = xmlOpeningTagParser.getXmlOpeningTagResult();
        if (xmlOpeningTagResult == null) {
            return openingTagPart;
        }
        StringBuilder sb = new StringBuilder(xmlOpeningTagResult.getTagName());
        List<String> attr = xmlOpeningTagResult.getAttributes();

        ArrayList<String> attributes = new ArrayList<String>(attr);
        Collections.sort(attributes, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String o1AttrName = o1.substring(0, o1.indexOf("="));
                String o2AttrName = o2.substring(0, o2.indexOf("="));
                int index1 = -1;
                int index2 = -1;
                for (int i = 0; i < MAIN_ATTR_NAMES.length; i++) {
                    String mainAttrName = MAIN_ATTR_NAMES[i];
                    if (mainAttrName.endsWith("*")) {
                        mainAttrName = mainAttrName.substring(0, mainAttrName.length() - 1);
                        if (o1AttrName.startsWith(mainAttrName)) {
                            index1 = i;
                        }
                        if (o2AttrName.startsWith(mainAttrName)) {
                            index2 = i;
                        }
                    } else {
                        if (o1AttrName.equals(mainAttrName)) {
                            index1 = i;
                        }
                        if (o2AttrName.equals(mainAttrName)) {
                            index2 = i;
                        }
                    }
                }
                if (index1 != index2) {
                    if (index1 == -1) {
                        return 1;
                    }
                    if (index2 == -1) {
                        return -1;
                    }
                    if (index1 < index2) {
                        return -1;
                    }
                    return 1;
                } else {
                    return o1.compareTo(o2);
                }
            }
        });

        if (attributes.size() == 1) {
            sb.append(" ").append(attributes.get(0));
        } else if (attributes.size() == 2) {
            sb.append(" ").append(attributes.get(0)).append(" ").append(attributes.get(1));
        } else if (attributes.size() > 2) {
            int attrIndent = 0;
            if (indentAttr >= 0) {
                attrIndent = indent + indentAttr;
            } else {
                attrIndent = indent + xmlOpeningTagResult.getTagName().length() + 1;
            }
            String spacesIndent = getSpacesIndent(attrIndent);
            for (int i = 0; i < attributes.size(); i++) {
                String attribute = attributes.get(i);
                if (!firstOfMultipleOnNewLine && i == 0) {
                    sb.append(" ");
                } else {
                    sb.append(lineSeparator);
                    sb.append(spacesIndent);
                }
                sb.append(attribute);
            }
        }

        if (xmlOpeningTagResult.isSelfclosing()) {
            sb.append("/");
        }
        sb.append(">");
        return sb.toString();
    }

    private static final String getSpacesIndent(int size) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(" ");
        }
        return sb.toString();
    }

    public void setFirstOfMultipleOnNewLine(boolean firstOfMultipleOnNewLine) {
        this.firstOfMultipleOnNewLine = firstOfMultipleOnNewLine;
    }

    public static void main(String[] args) {
        XmlOpeningTagGenerator xmlOpeningTagParser = new XmlOpeningTagGenerator("<beans xmlns=\"http://www.springframework.org/schema/beans\" xmlns:batch=\"http://www.springframework.org/schema/batch\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.springframework.org/schema/batch   http://www.springframework.org/schema/batch/spring-batch-3.0.xsd   http://www.springframework.org/schema/beans   http://www.springframework.org/schema/beans/spring-beans-4.2.xsd\">", 10, -1, "\n");
        String xmlOpeningTagResult = xmlOpeningTagParser.getOpeningTag();
        System.out.println("          " + xmlOpeningTagResult);
    }
}
