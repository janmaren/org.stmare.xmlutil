package com.stmare.str;

import java.util.MissingResourceException;
import java.util.ResourceBundle;

public class MsgUtil {
	public static String getMess(ResourceBundle bundle, String messageId)
	{
		if (bundle == null)
			return "[" + messageId + "]";
		String label = null;
		try {
			label = bundle.getString(messageId);
			return label;
		} catch (MissingResourceException e) {
			return "[" + messageId + "]";
		}
	}
}
