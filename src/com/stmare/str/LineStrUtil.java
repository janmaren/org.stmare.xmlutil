package com.stmare.str;


public class LineStrUtil {
    public static char[] escChars = {'\\', '.', '|', '-', '[', ']', '(', ')', '^', '$', '?', '*', '+', '{', '}', ':'};

    /**
     * @param str
     * @return escaped chars to be used in regular expression
     */
    public static String escapePattern(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            for (int j = 0; j < escChars.length; j++) {
                if (escChars[j] == ch) {
                    sb.append("\\");
                    break;
                }
            }
            sb.append(ch);
        }
        return sb.toString();
    }

    /**
     * @param fromPos
     * @param text
     * @return index to \r or \n
     */
    public static int findLastCharBeforeEndBackward(int fromPos, String text) {
        if (fromPos == 0)
            return 0;
        fromPos--;
        while (fromPos > 0) {
            char ch = text.charAt(fromPos);
            if (ch != '\r' && ch != '\n')
                return fromPos + 1;
            fromPos--;
        }
        return fromPos;
    }

    public static boolean isLineFeedBetween(String text, int start, int end) {
        for (int i = start; i < end; i++) {
            char ch = text.charAt(i);
            if (ch == '\r' || ch == '\n')
                return true;
        }
        return false;
    }

    /**
     * See also @{link {@link com.stmare.str.LineStrUtil#getLineRangeForPos(String, int)}
     * @param fromPos
     * @param text
     * @return cursor behind line feed. no change when already behind line feed
     */
    public static int findLineEnd(int fromPos, String text) {
        if (fromPos == 0)
            return 0;
        char ch = text.charAt(fromPos - 1);
        if (ch == '\r' || ch == '\n')
            return fromPos;
        
        fromPos++;
        while (fromPos < text.length()) {
            ch = text.charAt(fromPos);
            if (ch == '\r' || ch == '\n') {
                if (ch == '\n') {
                    return fromPos + 1;
                } else {
                    // ch == '\r'
                    if (fromPos + 1 < text.length()) {
                        // something behind '\r'
                        if (text.charAt(fromPos + 1) == '\n') {
                            return fromPos + 2;
                        } else {
                            return fromPos + 1;
                        }
                    }
                    return text.length();
                }
            }
            fromPos++;
        }
        return fromPos;
    }

    /**
     * See also @{link {@link com.stmare.str.LineStrUtil#getLineRangeForPos(String, int)}
     * @param fromPos
     * @param text
     * @return index of start of line which is after line feed or start of file
     */
    public static int findLineStart(int fromPos, String text) {
        if (fromPos == 0)
            return 0;
        fromPos--;
        while (fromPos > 0) {
            char ch = text.charAt(fromPos);
            if (ch == '\r' || ch == '\n')
                return fromPos + 1;
            fromPos--;
        }
        return fromPos;
    }

    /**
     * !!Prepsat podporujic MAC
     * @param str
     * @return
     */
    @Deprecated
    public static int countLineNumbers(String str) {
        if (str == null || str.length() == 0) {
            return 1;
        }
        int count = 1;
        int idx = 0;
        while ((idx = str.indexOf("\n", idx)) != -1) {
            count++;
            idx ++;
        }
        return count;
    }
    
    /**
     * !!Prepsat - podpora MAC
     * Vrati offset noveho radku, ktery je N radku za puvodnim offsetem (kdekoliv na radku)
     * @param text
     * @param fromOffset
     * @param lines
     * @return
     */
    @Deprecated
    public static int getOffsetLinesNext(String text, int fromOffset, int lines) {
        while (lines > 0) {
            fromOffset = text.indexOf("\n", fromOffset);
            if (fromOffset == -1) {
                return text.length();
            }
            lines --;
            fromOffset ++;
            if (fromOffset > text.length()) {
                fromOffset = text.length();
                break;
            }
        }
        return fromOffset;
    }

    /**
     * Najde zacatek soucasneho radku a zacatek dalsiho radku (pripadne konec souboru) 
     * @param string
     * @param pos
     * @return
     */
    public static int[] getLineRangeForPos(String string, int pos) {
        int i = pos - 1;
        while (i >= 0) {
            char charAt = string.charAt(i);
            if (charAt == '\r' || charAt == '\n') {
                break;
            }
            i--;
        }
        int begin = i + 1;
        
        i = pos;
        while (i < string.length()) {
            char charAt = string.charAt(i);
            if (charAt == '\r' || charAt == '\n') {
                if (i + 1 >= string.length()) {
                    i++;
                } else {
                    char charAt1 = string.charAt(i + 1);
                    if (charAt == '\r') {
                        if (charAt1 == '\n') {
                            i += 2;
                        } else {
                            i++;
                        }
                    } else {
                        // \n
                        i++;
                    }
                }
                break;
            }
            i++;
        }
        int end = i;
        
        return new int[] {begin, end};
    }
}
