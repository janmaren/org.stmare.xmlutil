package com.stmare.str;

import java.util.HashMap;
import java.util.Map;

public class CzechConvUtil {
    private final static char[][] diaToNodiaArray = {
        {'a', '\u00e1', '\u00e4'},
        {'A', '\u00c1', '\u00c4'},
        {'c', '\u010D', '\u0107'},
        {'C', '\u010C', '\u0106'},
        {'d', '\u010F'},
        {'D', '\u010E'},
        {'e', '\u011b', '\u00e9'},
        {'E', '\u011a', '\u00c9'},
        {'i', '\u00ed'},
        {'I', '\u00cd'},
        {'l', '\u013a', '\u013e'},
        {'L', '\u0139', '\u013d'},
        {'n', '\u0148', '\u0144', '\u00f1'},
        {'N', '\u0147', '\u0143', '\u00d1'},
        {'o', '\u00f3', '\u00f4', '\u00f6', '\u0151'},
        {'O', '\u00d3', '\u00d4', '\u00d6', '\u0150'},
        {'r', '\u0159', '\u0155'},
        {'R', '\u0158', '\u0154'},
        {'s', '\u0161', '\u015B'},
        {'S', '\u0160', '\u015A'},
        {'t', '\u0165'},
        {'T', '\u0164'},
        {'u', '\u00fa', '\u016f', '\u00fc', '\u0171'},
        {'U', '\u00da', '\u00dc', '\u016e', '\u0170'},
        {'y', '\u00fd'},
        {'Y', '\u00dd'},
        {'z', '\u017E', '\u017A'},
        {'Z', '\u017D', '\u0179'},
    };

    private final static Map<Character, Character> diaToNodiaMap = new HashMap<Character, Character>();
    private static String diaPatStr = null;

    static {
        StringBuilder sb = new StringBuilder();
        for (char[] nodia : diaToNodiaArray) {
            for (int i = 1; i < nodia.length; i++) {
                diaToNodiaMap.put(nodia[i], nodia[0]);
                sb.append(nodia[i]);
            }
        }
        diaPatStr = sb.toString();
    }

    /**
     * Return string of diacritics characters. Usefull for regular expression
     * 
     * @return
     */
    public static String getDiaPatStr() {
        return diaPatStr;
    }

    /**
     * Return no diacritics version of character
     * 
     * @param dia
     * @return
     */
    public static Character convertDiaToNodia(Character dia) {
        return diaToNodiaMap.get(dia);
    }
    
    /**
     * Return string withou diacritics
     * @param str
     * @return
     */
    public static String stripString(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charAt = str.charAt(i);
            Character stripedDia = convertDiaToNodia(charAt);
            sb.append(stripedDia == null ? charAt: stripedDia);
        }
        return sb.toString();
    }
}
