package com.stmare.str;

public class StrUtil {
    public static boolean startsWithIgnoreCase(String str, String startsWith) {
        if (str.length() < startsWith.length()) {
            return false;
        }
        String strLowerCase = str.toLowerCase();
        String startsWithLowerCase = startsWith.toLowerCase();
        return strLowerCase.startsWith(startsWithLowerCase);
    }

    public static String removeBlock(final String aInput, int fromPos, int toPosExcluding) {
        return aInput.substring(0, fromPos) + aInput.substring(toPosExcluding);
    }

    public static boolean isCharBetween(String temp, String ch, int from, int toExcluding) {
        String substring = temp.substring(from, toExcluding);
        return substring.contains(ch);
    }

    /**
     * Vraci true pokud string na danem indexu obsahuje danou sequenci zpet
     * @param str
     * @param index posledni pismeno sequence
     * @param sequence
     * @return
     */
    public static boolean isSequenceBack(String str, int index, String sequence) {
        boolean matches = true;
        for (int i = index, j = sequence.length() - 1; i > 0 && j > 0; i--, j--) {
            if (str.charAt(i) != sequence.charAt(j)) {
                matches = false;
                break;
            }
        }
        return matches;
    }

    public final static boolean isWhitespace(char ch) {
        return ch == '\r' || ch == '\n' || ch == '\t' || ch == ' ';
    }

    /**
     * Najde prvni pozici neprazdneho znaku vpred
     * @param str
     * @param fromPos
     * @return
     */
    public static int findNonWhiteSpacePosForward(String str, final int fromPos) {
        int index = fromPos;
        while (index < str.length()) {
            char charAt = str.charAt(index);
            if (isWhitespace(charAt)) {
                index ++;
            } else {
                return index;
            }
        }
        return index;
    }

    /**
     * Najde posledni pozici neprazdneho znaku zpet
     * @param str
     * @param fromPos
     * @return
     */
    public static int findNonWhiteSpacePosBackward(String str, final int fromPos) {
        int index = fromPos - 1;
        while (index >= 0) {
            char charAt = str.charAt(index);
            if (isWhitespace(charAt)) {
                index --;
            } else {
                return index;
            }
        }
        return -1;
    }
}
