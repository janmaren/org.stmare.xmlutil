/**
 * Copyright (c) 2010 Stanislav Marencik
 */
package org.stmare.xmlutil;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Platform;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.preferences.ConfigurationScope;
import org.eclipse.core.runtime.preferences.DefaultScope;
import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.framework.BundleContext;


/**
 * The activator class controls the plug-in life cycle
 */
@SuppressWarnings("unused")
public class Activator extends AbstractUIPlugin {

	// The plug-in ID
	public static final String PLUGIN_ID = "org.stmare.xmlutil";

	// The shared instance
	private static Activator plugin;

	/**
	 * The constructor
	 */
	public Activator() {
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		plugin = null;
		super.stop(context);
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}

	/**
	 * Returns an image descriptor for the image file at the given
	 * plug-in relative path
	 *
	 * @param path the path
	 * @return the image descriptor
	 */
	public static ImageDescriptor getImageDescriptor(String path) {
		return imageDescriptorFromPlugin(PLUGIN_ID, path);
	}

    public static void logDebug(String message) {
        // SimpleDateFormat simpleDateFormat = new SimpleDateFormat("h:m:s.S");
    	// plugin.getLog().log(new Status(IStatus.INFO, PLUGIN_ID, simpleDateFormat.format(new Date()) + ":" + message));  //$NON-NLS-1$

//        if (plugin.isDebugging()) {
//        	plugin.getLog().log(new Status(IStatus.INFO, PLUGIN_ID, IStatus.OK,
//                    message, null));
//        }
    }

    public static void logError(Throwable t) {
    	plugin.getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, "Internal Error in org.stmare plugin", t));  //$NON-NLS-1$
    }

    public static void logError(Throwable t, String message) {
        plugin.getLog().log(new Status(IStatus.ERROR, PLUGIN_ID, IStatus.ERROR, message, t));  //$NON-NLS-1$
    }

    public static void logInfo(String message) {
    	plugin.getLog().log(new Status(IStatus.INFO, PLUGIN_ID, message));  //$NON-NLS-1$
    }

    public static void logWarn(String message) {
    	//plugin.getLog().log(new Status(IStatus.WARNING, PLUGIN_ID, message));  //$NON-NLS-1$
    }

    public static void logWarn(Throwable t) {
    	//plugin.getLog().log(new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, "Caught exception", t));  //$NON-NLS-1$
    }

    public static void logWarn(Throwable t, String message) {
    	//plugin.getLog().log(new Status(IStatus.WARNING, PLUGIN_ID, IStatus.WARNING, message, t));  //$NON-NLS-1$
    }

    public static boolean isSpacesForTabs() {
        IEclipsePreferences nodeUi = InstanceScope.INSTANCE.getNode("org.eclipse.ui.editors");
        return nodeUi.getBoolean("spacesForTabs", false);
    }
    
    public static int getTabSize() {
        IEclipsePreferences nodeUi = InstanceScope.INSTANCE.getNode("org.eclipse.ui.editors");
        return nodeUi.getInt("tabWidth", 4);
//        int tab = 4;
//        IEclipsePreferences node = new DefaultScope().getNode("org.eclipse.wst.xml.core");
//        if (node == null) {
//            return tab;
//        }
//        String iChar = node.get("indentationChar", "space");
//        if ("tab".equals(iChar)) {
//            return 0;
//        }
//        tab = node.getInt("indentationSize", 4);
//        return tab;
    }

    public static String getLineDelimiter() {
        IScopeContext[] scopeContext = new IScopeContext[] { InstanceScope.INSTANCE, ConfigurationScope.INSTANCE, DefaultScope.INSTANCE };
        String lineSeparator = Platform.getPreferencesService().getString(
                Platform.PI_RUNTIME, Platform.PREF_LINE_SEPARATOR, null,
                scopeContext);
        return lineSeparator != null ? lineSeparator : System.getProperty("line.separator");
    }

}
