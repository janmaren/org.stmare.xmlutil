package org.stmare.xmlutil;

import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.TextSelection;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.stmare.xml.format.RangeXmlFormatter;
import com.stmare.xml.format.TransformerXmlFormatter;
import com.stmare.xml.format.XmlFormatConfig;
import com.stmare.xml.format.XmlFormatResult;

public class Formatter {
    public static void format(IDocument document, ITextSelection textSelection, ISelectionProvider selectionProvider) {
        XmlFormatConfig config = createConfig();
        String xml = document.get();
        RangeXmlFormatter rangeXmlFormatter = new RangeXmlFormatter(xml, textSelection.getOffset(), textSelection.getLength(), config);
        XmlFormatResult formattedXml = rangeXmlFormatter.getFormattedXml();
        if (!formattedXml.isFormatted()) {
            String resultMessage = formattedXml.getResultMessage();
            Shell shell = new Shell(Display.getDefault());
            MessageDialog.openError(shell, "Formatting Error", resultMessage != null ? resultMessage : "Unable to format");
            return;
        }
        try {
            String selectedXml = xml.substring(textSelection.getOffset() + formattedXml.getStartMovedDelta(), textSelection.getOffset() + textSelection.getLength() + formattedXml.getEndMovedDelta());
            if (selectedXml.equals(formattedXml.getFormattedXmlSelection())) {
                return;
            }
            document.replace(textSelection.getOffset() + formattedXml.getStartMovedDelta(), textSelection.getLength() - formattedXml.getStartMovedDelta() + formattedXml.getEndMovedDelta(), formattedXml.getFormattedXmlSelection());
            TextSelection newTextSelection = new TextSelection(textSelection.getOffset() + formattedXml.getStartMovedDelta(), formattedXml.getFormattedXmlSelection().length());
            selectionProvider.setSelection(newTextSelection);
        } catch (BadLocationException e) {
            Activator.logError(e);
        }
    }

    public static void format(IDocument document) {
        XmlFormatConfig config = createConfig();
        String xml = document.get();
        TransformerXmlFormatter transformerXmlFormatter = new TransformerXmlFormatter();
        transformerXmlFormatter.setXmlFormatConfig(config);
        String formattedXml = transformerXmlFormatter.getFormattedXml(xml);
        if (formattedXml == null) {
            String resultMessage = transformerXmlFormatter.getResultMessage();
            Shell shell = new Shell(Display.getDefault());
            MessageDialog.openError(shell, "Formatting Error", resultMessage != null ? resultMessage : "Unable to format");
            return;
        }
        if (xml.equals(formattedXml)) {
            return;
        }
        try {
            document.replace(0, xml.length(), formattedXml);
        } catch (BadLocationException e) {
            Activator.logError(e);
        }
    }
    
    public static XmlFormatConfig createConfig() {
        XmlFormatConfig xmlFormatConfig = new XmlFormatConfig();
        xmlFormatConfig.setIndent(Activator.getTabSize());
        xmlFormatConfig.setUseTabs(!Activator.isSpacesForTabs());
        xmlFormatConfig.setLineFeed(Activator.getLineDelimiter());
        xmlFormatConfig.setKeepEmptyLines(true);
        return xmlFormatConfig;
    }
}
