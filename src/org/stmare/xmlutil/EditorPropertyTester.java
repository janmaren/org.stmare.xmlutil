package org.stmare.xmlutil;

import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.stmare.str.StrUtil;

public class EditorPropertyTester extends org.eclipse.core.expressions.PropertyTester {

    @Override
    public boolean test(Object object, String testName, Object[] arg2, Object value) {
        IWorkbenchPart activePart = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().getActivePart();
        if (activePart instanceof IEditorPart) {
            ITextEditor textEditor = (ITextEditor) activePart.getAdapter(ITextEditor.class);
            if (textEditor != null) {
                IDocumentProvider documentProvider = textEditor.getDocumentProvider();
                IDocument document = documentProvider.getDocument(((IEditorPart) activePart).getEditorInput());
                String string = document.get();
                int findNonWhiteSpacePosBackward = StrUtil.findNonWhiteSpacePosBackward(string, 0) + 1;
                if (findNonWhiteSpacePosBackward < string.length()) {
                    char charAt = string.charAt(findNonWhiteSpacePosBackward);
                    if (charAt == '<') {
                        return true;
                    }
                }
            }            
        } 
        return false;
    }
}
