package org.stmare.util;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IClassFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.TreePath;
import org.eclipse.jface.viewers.TreeSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.ISelectionService;
import org.eclipse.ui.PlatformUI;

public class HandlerUtil {

    public static IFile findSelectedIFileInTree() {
        ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
        ISelection selection = selectionService.getSelection();
        if (selection instanceof TreeSelection) {
            TreeSelection treeSelection = (TreeSelection) selection;
            TreePath[] paths = treeSelection.getPaths();
            TreePath treePath = paths[0];
            Object lastSegment = treePath.getLastSegment();
            if (lastSegment instanceof IFile) {
                return (IFile) lastSegment;
            }
            if (lastSegment instanceof IAdaptable) {
                IAdaptable iAdaptable = (IAdaptable) lastSegment;
                Object adapted = iAdaptable.getAdapter(IResource.class);
                if (adapted instanceof IFile) {
                    return (IFile) adapted;
                }
            }
        }
        return null;
    }
    
    public static String findSelectedClassInTree() {
        ISelectionService selectionService = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getSelectionService();
        ISelection selection = selectionService.getSelection();
        if (selection instanceof TreeSelection) {
            TreeSelection treeSelection = (TreeSelection) selection;
            TreePath[] paths = treeSelection.getPaths();
            TreePath treePath = paths[0];
            Object lastSegment = treePath.getLastSegment();
            if ((lastSegment instanceof org.eclipse.jdt.core.IClassFile)) {
                return ((org.eclipse.jdt.core.IClassFile) lastSegment).getType().getFullyQualifiedName();
            }
        }
        return null;
    }
    
    public static IFile findSelectedIFileInEditor(IEditorPart editorPart) {
        IResource resource = (IResource)((IEditorPart) editorPart).getEditorInput().getAdapter(IResource.class);
        if (resource instanceof IFile) {
            return (IFile) resource;
        }
        resource = (IResource) Platform.getAdapterManager().getAdapter(editorPart, IResource.class);
        if (resource instanceof IFile) {
            return (IFile) resource;
        }
        return null;
    }
    
    public static String findSelectedClassNameInEditor(IEditorPart editorPart) {
        IClassFile classFile = (IClassFile) editorPart.getEditorInput().getAdapter(IClassFile.class);
        if (classFile != null) {
            return classFile.getType().getFullyQualifiedName();
        }

        return null;
    }

    /**
     * @param referencedFile must be a java file with ".java" suffix
     * @return
     */
    public static String findClassName(IFile referencedFile) {
        String referencedName = referencedFile.getName();
        if (!referencedName.endsWith(".java")) {
            throw new IllegalArgumentException("Other than java file as argument");
        }
        ICompilationUnit iCompilationUnit = (ICompilationUnit) JavaCore.create(referencedFile);
        if (iCompilationUnit == null) {
            return null;
        }
        IType type = iCompilationUnit.getType(referencedName.substring(0, referencedName.length() - ".java".length()));
        return type.getFullyQualifiedName();
    }
}
